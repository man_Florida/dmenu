/* See LICENSE file for copyright and license details. */
/* comment for git test */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"Roboto:size=10"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
	/*					fg         bg       */
	//[SchemeNorm] = { "#bbc2cf", "#282c34" }, /* Normal color scheme*/
	//[SchemeNorm] = { "#bbc2cf", "#434758" }, /* Normal color scheme*/
	//[SchemeNorm] = { "#bbc2cf", "#1e1c31" }, /* Normal color scheme*/
	[SchemeNorm] = { "#bbc2cf", "#100e23" }, /* Normal color scheme*/
	[SchemeSel] = { "#100e23", "#51afef" },  /* Selected colro */
	[SchemeSelHighlight]  { "#100e23", "#ffe585" }, //fuzzy highlight fg, bg
	[SchemeNormHighlight] = { "#ffc978", "#222222" },
	[SchemeOut] = { "#000000", "#00ffff" },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;
/* -h option; minimum height of a menu line */
static unsigned int lineheight = 0;
static unsigned int min_lineheight = 8;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
